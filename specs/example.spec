# Acceptance Test for ToDo App

To execute this specification, use
	npm test

This is a context step that runs before every scenario
* Open todo application

## Add To Do
* Add task "Add Task Test"
* Must display "Add Task Test"

## Must stay to do item when browser restarted
* Add task "This Task Shouldn't Disappear"
* Open todo application
* Must display "This Task Shouldn't Disappear"

___
* Clear all tasks
