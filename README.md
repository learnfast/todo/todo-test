# ToDo Project / Acceptance Test

## Project Overview

---

This is an acceptance test for ToDo project written in Gauge + Taiko.

## Usage

---

### `gauge run`

This command starts the acceptance test. If you want to see more information about test, you can look `specs/example.spec` file.

## CI/CD

---

There are 2 stages in pipeline,

- containerize
- run

Docker image of this project saved to registry.gitlab.com/learnfast/todo/todo-test/master:latest

When API service or GUI pushed to GIT, they run this test automatically. You can use command below for run the pipeline but you need trigger token.
```sh
curl -X POST -F token=$ACCEPTANCE_TOKEN -F ref=master https://gitlab.com/api/v4/projects/27131729/trigger/pipeline
```
